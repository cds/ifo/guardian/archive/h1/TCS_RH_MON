#Nutsinee K. 13th Jan 2017
#editted Nutsinee K. 8 May 2017: commented out log, moved return outside the for loop.
from guardian import GuardState, GuardStateDecorator
import time

nominal = 'RH_OKAY'
request = nominal
# Two states: RH OKAY and FAULT


###################################################################
# Funtions

def in_fault():
    optics = ['ITMX','ITMY','ETMX','ETMY']
    
    # upper ring heater
    for optic in optics:
        upperpower = ezca['TCS-{}_RH_UPPERPOWER'.format(optic)]
        setupperpower = ezca['TCS-{}_RH_SETUPPERPOWER'.format(optic)]

        if setupperpower <= 0.5:
            if abs(setupperpower-upperpower) > 0.03:
                notify('{} Upper Ring Heater maybe dead'.format(optic))
                return True
        elif setupperpower > 0.5:
            if abs(setupperpower-upperpower) > 0.1*setupperpower:
                notify('{} Upper Ring Heater maybe dead'.format(optic))
                return True

    # lower ring heater
    for optic in optics:
        lowerpower = ezca['TCS-{}_RH_LOWERPOWER'.format(optic)]
        setlowerpower = ezca['TCS-{}_RH_SETLOWERPOWER'.format(optic)]

        if setlowerpower <= 0.5:
            if abs(setlowerpower-lowerpower) > 0.03:
                notify('{} Lower Ring Heater maybe dead'.format(optic))
                return True
        elif setlowerpower > 0.5:
            if abs(setlowerpower-lowerpower) > 0.1*setlowerpower:
                notify('{} Lower Ring Heater maybe dead'.format(optic))
                return True

            
###################################################################
# Decorators

class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_fault():
            return 'FAULT'

###################################################################
# States

class INIT(GuardState):
    def main(self):
        if in_fault():
            return 'FAULT'
        else:
            return 'RH_OKAY'

class FAULT(GuardState):
    index = 10
    def run(self):
        if in_fault():
            return False
        else:
            return True
        
class RH_OKAY(GuardState):
    index = 20
    @fault_checker
    def run(self):
        return True

###################################################################
# Edges

edges = [
    ('INIT','RH_OKAY'),
    ('INIT','FAULT'),
    ('FAULT','RH_OKAY'),
    ('RH_OKAY','FAULT'),
]
